import java.util.Scanner; 

public class Konsoleneingaben {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine ganze Zahl ein:");
		
		int zahl1 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein:");
		
		int zahl2 = myScanner.nextInt();
		
		int ergebnis = zahl1 + zahl2;
		
		System.out.print("\n\n\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + "+" + zahl2 + "=" + ergebnis);
		
		int ergebnis2 = zahl1 - zahl2;
		
		System.out.print("\nErgebnis der Subtraktion lautet: ");
		System.out.print(zahl1 + "-" + zahl2 + "=" + ergebnis2);
		
		int ergebnis3 = zahl1 * zahl2;
		
		System.out.print("\nErgebnis der Multiplikation lautet: ");
		System.out.print(zahl1 + "*" + zahl2 + "=" + ergebnis3);
		
		int ergebnis4 = zahl1 / zahl2;
		
		System.out.print("\nErgebnis der Division lautet: ");
		System.out.print(zahl1 + "/" + zahl2 + "=" + ergebnis4);
		
		
		
		myScanner.close();
		
	
		
	}

}
